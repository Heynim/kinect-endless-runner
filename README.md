# Simple Kinect Endless Runner #

## Summary ##

A simple endless runner made for university thesis utilizing Microsoft Kinect v2 as a controller. The player must run in place to move forward and collect candy while avoiding obstacles by moving side-to-side.

![2017-06-05 14_06_19-BAB I - skripsi hanim [FINAL] revisi 3.pdf.png](https://bitbucket.org/repo/B7EjjA/images/2423089689-2017-06-05%2014_06_19-BAB%20I%20-%20skripsi%20hanim%20%5BFINAL%5D%20revisi%203.pdf.png)

## Dependencies ##
* Unity 5.6.1f1 or above.
* Microsoft Kinect v2 (Microsoft Kinect for Xbox One).
* Kinect for Windows SDK 2.0: [https://www.microsoft.com/en-sg/download/details.aspx?id=44561](https://www.microsoft.com/en-sg/download/details.aspx?id=44561).

## Setup ##
* Clone or Download to desktop.
* Open in Unity.
* Run Scenes > sceneMain.

## Usage & Controls ##
Make sure the Kinect is detected, then stand in front of the Kinect until both indicators at the top left of the screen are green.

![2017-06-05 14_02_34-Clipboard.png](https://bitbucket.org/repo/B7EjjA/images/2910988391-2017-06-05%2014_02_34-Clipboard.png)

Use the mouse to click the "Start" button. In-game controls are defined as follows:

* Mouse to interact with menus.
* Run in place to move forward.
* Move sideways for lateral movement.

===