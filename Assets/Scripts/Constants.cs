﻿
// Constants reference.
//
public class Constants {

    // Tag obyek pemain.
    public static readonly string PlayerTag = "Player";
    // Tag obyek jalan.
    public static readonly string PathTag = "Path";
    // Tag obyek rintangan & skor.
    public static readonly string DestroyerTag = "Destroyer";

    // Nama parameter animasi memulai.
    public static readonly string AnimationStarted = "started"; 
}

