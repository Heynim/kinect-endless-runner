﻿
// All possible states of the game.
//
public enum GameState {

    Start,
    Playing,
    Dead
}
