﻿using UnityEngine;

// Script to manage Game State.
//
public class GameManager : MonoBehaviour {

    // Asign GameManager on Awake, Destroy if already exists.
    void Awake() {

        if (instance == null) {
            instance = this;
        } else {
            DestroyImmediate(this);
        }
    }

    // GameManager instance.
    private static GameManager instance;
    // Get intance. Instantiate if null.
    public static GameManager Instance {

        get {
            if (instance == null) {
                instance = new GameManager();
            }
            return instance;
        }
    }

    // Constructor.
    protected GameManager() {

        GameState = GameState.Start;
        KinectDetected = true;
        PlayerDetected = true;
    }

    // Current Game State.
    public GameState GameState { get; set; }

    // Kinect Status.
    [SerializeField]
    private bool KinectDetected;
    // Player Status.
    [SerializeField]
    private bool PlayerDetected;
    
    // Player has died, Set GameState to Dead.
    public void Die() {
        
        this.GameState = GameState.Dead;
        GetComponent<AudioSource>().Stop();
        UIManager.Instance.ShowGameOver();
    }

    // Is the Kinect detected?
    public void SetKinectStatus(bool status) {

        if (status) {

            KinectDetected = true;
            UIManager.Instance.SetKinectStatusText(true);
        } 
        else {

            KinectDetected = false;
            UIManager.Instance.SetKinectStatusText(false);
        }
    }

    // Is the Player detected?
    public void SetPlayerStatus(bool status) {

        if (status) {

            PlayerDetected = true;
            UIManager.Instance.SetPlayerStatusText(true);
        } else {

            PlayerDetected = false;
            UIManager.Instance.SetPlayerStatusText(false);
        }
    }

    // Is the game ready? (Ready if both Kinect & Player is detected).
    public bool GameReady() {

        if (KinectDetected && PlayerDetected) {
            UIManager.Instance.StartButton.interactable = true;
            return true;
        } 
        else {
            UIManager.Instance.StartButton.interactable = false;
            return false;
        }
    }
}
