﻿using UnityEngine;
using UnityEngine.UI;

// Script to manage all UI interaction.
//
public class UIManager : MonoBehaviour {

    // Asign UIManager on Awake, Destroy if already exists.
    void Awake() {

        if (instance == null) {
            instance = this;
        } else {
            DestroyImmediate(this);
        }
    }

    // UIManager instance.
    private static UIManager instance;
    // Get intance. instantiate if null.
    public static UIManager Instance {

        get {
            if (instance == null) {
                instance = new UIManager();
            }
            return instance;
        }
    }

    // Constructor.
    protected UIManager() { }

    // UI.
    public Text ScoreText;
    public Image StartMenu;
    public Image GameOverMenu;

    // Button.
    public Button StartButton;

    // Debug UI.
    public Text KinectStatusText;
    public Text PlayerStatusText;

    // Set initial text.
    void Start() {

        ResetScore();
        UpdateScoreText();
    }

    // Score.
    private float score = 0;

    // Reset Score.
    public void ResetScore() {

        score = 0;
        UpdateScoreText();
    }

    // Set Score.
    public void SetScore(float value) {

        score = value;
        UpdateScoreText();
    }

    // Increase Score.
    public void IncreaseScore(float value) {

        score += value;
        UpdateScoreText();
    }

    // Update Score Text.
    public void UpdateScoreText() {

        ScoreText.text = score.ToString("0");
    }

    // Hide Start Menu.
    public void HideStartMenu() {
        StartMenu.gameObject.SetActive(false);
    }

    // Show Game Over.
    public void ShowGameOver() {
        GameOverMenu.gameObject.SetActive(true);
    }

    // Set Kinect Status.
    public void SetKinectStatusText(bool status) {

        if (status)
            KinectStatusText.text = "<color=green>Kinect Detected</color>";
        else
            KinectStatusText.text = "<color=red>Kinect Not Detected!</color>";
    }

    // Set Player Status.
    public void SetPlayerStatusText(bool status) {

        if (status)
            PlayerStatusText.text = "<color=green>Player Detected</color>";
        else
            PlayerStatusText.text = "<color=red>Player Not Detected!</color>";
    }
}
