﻿using UnityEngine;
using System.Collections;

// Script to clean up passed objects.
//
public class ObjDestroyer : MonoBehaviour {
	
    // Destroy object once it has left Player FOV.
	void OnTriggerEnter(Collider col) {

        if (col.gameObject.tag == Constants.DestroyerTag) {
            Destroy(this.gameObject);
        }
    }
}

