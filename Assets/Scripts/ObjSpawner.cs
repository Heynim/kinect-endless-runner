﻿using UnityEngine;

// Script to spawn pickups on every Path segment.
//
public class ObjSpawner : MonoBehaviour {

    // Positions where stuff will spawn.
    public Transform[] StuffSpawnPoints;
    // Good GameObjects.
    public GameObject[] Bonus;
    // Bad GameObjects.
    public GameObject[] Obstacles;

    // Set true to randomize X position.
    public bool RandomX = false;
    public float minX = -3f, maxX = 3f;

    // Create a pickup at [position] of type [prefab]. Pickup can be Bonus or Obstacle.
    void CreateObject(Vector3 position, GameObject prefab) {

        if (RandomX)
            position += new Vector3(Random.Range(minX, maxX), 0, 0);

        Instantiate(prefab, position, Quaternion.identity);
    }

    void Start() {
        // Set the probability of an Obstacle & Bonus spawning.
        bool placeObstacle = Random.Range(0, 1) == 0;   // 50% chance of Obstacle spawn.
        bool placeBonus = Random.Range(0, 2) == 0;      // 33% chance of Bonus spawn.

        // Spawn an Obstacle if true.
        int obstacleIndex = -1;
        if (placeObstacle) {

            // Select a random spawn point other than first one.
            obstacleIndex = Random.Range(1, StuffSpawnPoints.Length);
            // Create Obstacle.
            CreateObject(StuffSpawnPoints[obstacleIndex].position, Obstacles[Random.Range(0, Obstacles.Length)]);
        }

        // Spawn a Bonus object in remaining spawn points.
        for (int i = 0; i < StuffSpawnPoints.Length; i++) {
            // Don't instantiate if there's an obstacle at that spawn point.
            if (i == obstacleIndex) continue;

            if (placeBonus)
                CreateObject(StuffSpawnPoints[i].position, Bonus[Random.Range(0, Bonus.Length)]);
        }
    }
}

