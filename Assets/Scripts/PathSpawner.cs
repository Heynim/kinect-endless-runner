﻿using UnityEngine;

// Script to spawn Path segment to simulate an infinite path.
//
public class PathSpawner : MonoBehaviour {
    
    // Position of next Path spawn point.
    public Transform PathSpawnPoint;
    // Path prefab.
    public GameObject Path;
    // Previous Path.
    private GameObject prevPath;
    // Path life time.
    public float PathLifeTime = 1f;

    // On Player collision, create a new Path segment at the next Path spawn point.
    void OnTriggerEnter(Collider col) {

        if (col.gameObject.tag == Constants.PlayerTag) {

            // Destroy previous Path if prevPath not null.
            if (prevPath) {
                Invoke("DestroyPrev", PathLifeTime);
            }

            // Instantiate Path on PathSpawnPoint.
            GameObject newPath = Instantiate(Path, PathSpawnPoint.position, PathSpawnPoint.rotation) as GameObject;
            newPath.GetComponentInChildren<PathSpawner>().prevPath = this.transform.root.gameObject;
        }
    }

    // Destroy previous Path.
    void DestroyPrev() {

        Destroy(prevPath);
    }
}

