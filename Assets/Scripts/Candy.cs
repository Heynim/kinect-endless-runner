﻿using UnityEngine;
using System.Collections;

// Candy object behaviour.
//
public class Candy : MonoBehaviour {

    // Score.
    public int scorePoints = 100;
    // Y-axis rotation speed.
    public float rotateSpeed = 50f;
    // Sound effect reference.
    private AudioClip soundEffect;

    void Start() {

        soundEffect = GetComponent<AudioSource>().clip;
    }

    // Continuously rotate on Y-axis.
    void Update() {

        transform.Rotate(Vector3.up, Time.deltaTime * rotateSpeed);
    }

    // On Player collision, add score and destroy gameObject.
    // Otherwise, destroy gameObject on collision with Destroyer object.
    void OnTriggerEnter(Collider col) {

        if (col.gameObject.tag == Constants.PlayerTag) {
            UIManager.Instance.IncreaseScore(scorePoints);
            AudioSource.PlayClipAtPoint(soundEffect, transform.position);
            Destroy(this.gameObject);
        }

        if (col.gameObject.tag == Constants.DestroyerTag) {
            Destroy(this.gameObject);
        }
    }
}


