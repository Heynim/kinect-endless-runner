﻿using UnityEngine;
using Windows.Kinect;
using UnityEngine.SceneManagement;

// Script to manage Character movement using Kinect.
//
public class CharacterKinectMovement : MonoBehaviour {
    
    /// <summary>
    /// Character-related variables
    /// </summary>
    // Move direction in x, y, and z-axis.
    private Vector3 moveDirection = Vector3.zero;
    // Strength of gravity.
    public float gravity = 20f;
    // Jump Speed.
    public float JumpSpeed = 8.0f;
    // Run Speed.
    public float Speed = 6.0f;
    // Score per Second.
    public float scorePerSecond = 10f;

    // Reference to Character Controller and Animation Controller.
    private CharacterController controller;
    private Animator anim;
    // Reference to Character gameObject.
    public Transform Character;


    /// <summary>
    /// Kinect-related variables.
    /// </summary>
    public GameObject BodySourceManager;

    private ulong _BodyId;
    private RunGesture _gesture;
    private BodySourceManager _BodyManager;

    private int trackedIndex = -1;      // Index of tracked Body.
    private float gestureTimeout = 1f;  // How long to wait for the next gesture segment in the sequence.
    private float gestureTimer = 0f;    // Timer to countdown gesture timeout.
    private float gameOverTimer = 5f;   // Time to Game Over if no gesture is made.


    /// <summary>
    /// Methods
    /// </summary>
    void Start() {

        GameManager.Instance.GameState = GameState.Start;   // Honestly, this probably should've been in GameManager.

        controller = GetComponent<CharacterController>();
        anim = Character.GetComponent<Animator>();
    }

    // Die if character falls.
    private void CheckHeight() {

        if (transform.position.y < -10) {
            GameManager.Instance.Die();
        }
    }

    // Update every frame.
    void Update() {

        // Initialize and Refresh Kinect Data.
        Body[] data = Kinect_Init();

        // Check if Kinect is detected.
        if (data == null) {
            GameManager.Instance.SetKinectStatus(false);
        } else {
            GameManager.Instance.SetKinectStatus(true);
        }

        // Check if Player is tracked.
        if (trackedIndex == -1) {
            GameManager.Instance.SetPlayerStatus(false);
        } else {
            GameManager.Instance.SetPlayerStatus(true);
        }

        // Execute behaviour based on Game State.
        // Once again, this probably should've been independent from the Character and be placed in GameManager. :(
        switch (GameManager.Instance.GameState) {

            case GameState.Start:

                // Check if Game is Ready.
                if (!GameManager.Instance.GameReady()) {
                    return;
                }
                break;
            case GameState.Playing:

                // Check if character fell.
                CheckHeight();

                // Set horizontal position of Character based on horizontal position of [SpineMid].
                float xPos = data[trackedIndex].Joints[JointType.SpineMid].Position.X * 9;
                transform.position = Vector3.Lerp(transform.position, new Vector3(Mathf.Clamp(xPos, -3, 3), transform.position.y, transform.position.z), Speed * Time.deltaTime);
                //transform.position = new Vector3(Mathf.Clamp(xPos, -3, 3), transform.position.y, transform.position.z);

                // Running gesture detection.
                // Reset timer if gesture detected, decrement otherwise.
                bool? gestureDetected = Gesture_GestureDetect(data);
                if (gestureDetected == null) {
                    return;
                }
                else if (gestureDetected == true) {
                    gestureTimer = gestureTimeout;
                } 
                else {

                    if(gestureTimer > -1)
                        gestureTimer -= 1 * Time.deltaTime; Debug.Log("Timer: " + gestureTimer);
                }
                
                // Add forward movement if timer > 0.
                if (gestureTimer > 0) {

                    // Get character local forward direction.
                    moveDirection = transform.forward;
                    // Transform local forward into world forward.
                    moveDirection = transform.TransformDirection(moveDirection);
                    // Multiply by Speed.
                    moveDirection *= Speed;

                    // Increase score when running.
                    UIManager.Instance.IncreaseScore(scorePerSecond * Time.deltaTime);

                    // Reset Game Over timer.
                    gameOverTimer = 5f;
                }
                // Reset forward movement otherwise.
                else {

                    if (moveDirection.z > 0) {
                        moveDirection = Vector3.Lerp(moveDirection, new Vector3(moveDirection.x, moveDirection.y, 0), Speed);
                    }

                    if (gameOverTimer > 0) {
                        gameOverTimer -= (1 * Time.deltaTime);
                    }
                    else {
                        GameManager.Instance.Die();
                    }
                } Debug.Log(gameOverTimer);

                // Apply Gravity.
                moveDirection += Physics.gravity;
                
                // Move Character forward.
                controller.Move(moveDirection * Time.deltaTime);

                break;
            case GameState.Dead:

                anim.SetBool(Constants.AnimationStarted, false);
                break;
            default:
                break;
        }
    }

    // Initialize and Refresh Kinect Data.
    private Body[] Kinect_Init() {

        if (BodySourceManager == null) {
            return null;
        }

        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null) {
            return null;
        }

        // Get Body data.
        Body[] data = _BodyManager.GetData();
        if (data == null) {
            return null;
        }

        // Check if trackedIndex is still active. i.e., if a Body is being tracked.
        // If not, reset trackedIndex.
        if (trackedIndex != -1) {
            Body body = data[trackedIndex];
            if (!body.IsTracked) {
                trackedIndex = -1;
            }
        }

        // Get new trackedIndex if not active.
        if (trackedIndex == -1) {
            // Get first tracked Body's index.
            for (int i = 0; i < data.Length; i++) {
                if (data[i] == null) {
                    continue;
                }

                if (data[i].IsTracked) {
                    trackedIndex = i;
                    break;
                }
            }
        }

        return data;
    }

    // Gesture detection. Check if Player is running in place.
    private bool? Gesture_GestureDetect(Body[] data) {
                
        // If active body still active after check and update, use it.        
        if (trackedIndex != -1) {
            if (_BodyId != data[trackedIndex].TrackingId) {
                _BodyId = data[trackedIndex].TrackingId;

                _gesture = new RunGesture();
                _gesture.GestureRecognized += Gesture_GestureRecognized;
            }

            bool gestureRecognized = _gesture.Update(data[trackedIndex]);
            if (gestureRecognized) {
                return true;
            }
            Debug.Log("Segment: " + _gesture._currentSegment);

        }

        return false;
    }
    
    // Gesture recognized event.
    static void Gesture_GestureRecognized(object sender, GestureRecognizedEventArgs e) {
        Debug.Log("Body: " + e.BodyId + "<b><color=red> just ran!</color></b>");
    }

    // Start Game.
    public void StartGame() {

        UIManager.Instance.HideStartMenu();
        anim.SetBool(Constants.AnimationStarted, true);
        GameManager.Instance.GameState = GameState.Playing;
    }

    // Restart Game.
    public void RestartGame() {

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
