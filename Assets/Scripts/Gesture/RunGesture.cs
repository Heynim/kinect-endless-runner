﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Windows.Kinect;

// Event handler arguments.
//
public class GestureRecognizedEventArgs : EventArgs {

    public ulong BodyId { get; set; }

    public GestureRecognizedEventArgs(ulong BodyId) { this.BodyId = BodyId; }
}

// Script to detect if Player is running in place.
//
class RunGesture {

    // Wait time per gesture segment in seconds.
    readonly float WINDOW_TIME = 0.5f;

    // Array of gesture sequence.
    IGestureSegment[] _segments;

    // Current detected segment.
    public int _currentSegment = 0;
    // Time passed since last segment detected.
    public float _timeCount = 0;

    // Event handler when gesture recognized.
    public event EventHandler<GestureRecognizedEventArgs> GestureRecognized;

    // Constructor.
    public RunGesture() {

        // Initialize gesture segments.
        RightKneeUp rightKneeUp = new RightKneeUp();
        LeftKneeUp leftKneeUp = new LeftKneeUp();

        // Set sequence of gesture segments. i.e., what qualifies as a run gesture?
        _segments = new IGestureSegment[] {

            rightKneeUp,
            leftKneeUp,
            rightKneeUp
        };
    }

    // Main gesture checker.
    public bool Update(Body body) {

        GesturePartResult result = _segments[_currentSegment].Update(body);

        // Check if gesture segment is detected.
        if (result == GesturePartResult.Succeeded) {

            // Gesture segment is detected but not the end of gesture sequence. Check next segment during next loop.
            if (_currentSegment + 1 < _segments.Length) {

                _currentSegment++;
                _timeCount = 0;
            }
            // End of gesture sequence. i.e., run gesture is valid. Trigger GestureRecognized event and reset.
            else {

                if (GestureRecognized != null) {

                    GestureRecognized(this, new GestureRecognizedEventArgs(body.TrackingId));
                    Reset();
                    return true;
                }
            }
        }
        // Gesture segment not detected within time window, reset.
        else if (_timeCount > WINDOW_TIME) {
            Reset();
        } 
        // Increment timer.
        else {
            _timeCount += 1 * Time.deltaTime;
        }

        return false;
    }

    // Reset segment and timer.
    public void Reset() {

        _currentSegment = 0;
        _timeCount = 0;
    }
}

