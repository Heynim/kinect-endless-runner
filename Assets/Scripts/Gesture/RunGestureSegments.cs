﻿using Windows.Kinect;

// Gesture segment interface.
//
public interface IGestureSegment {

    GesturePartResult Update(Body body);
}

// Right knee up.
//
public class RightKneeUp : IGestureSegment {

    public GesturePartResult Update(Body body) {

        if (body.Joints[JointType.KneeRight].Position.Y > body.Joints[JointType.KneeLeft].Position.Y + 0.005)
            return GesturePartResult.Succeeded;
        else
            return GesturePartResult.Failed;
    }
}


// Left knee up.
//
public class LeftKneeUp : IGestureSegment {

    public GesturePartResult Update(Body body) {

        if (body.Joints[JointType.KneeLeft].Position.Y > body.Joints[JointType.KneeRight].Position.Y + 0.005)
            return GesturePartResult.Succeeded;
        else
            return GesturePartResult.Failed;
    }
}

