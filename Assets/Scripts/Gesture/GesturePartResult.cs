﻿
// All possible detection states of gesture segments.
//
public enum GesturePartResult {

    Failed,
    Succeeded,
    Undetermined
}