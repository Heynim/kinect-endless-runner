﻿using UnityEngine;
using System.Collections;

// Script to move camera relative to object on Z-axis only.
//
public class CameraFollowZ : MonoBehaviour {

    // Object to follow.
    public Transform objectToFollow;
    // Lerp Speed.
    public float lerpSpeed;
    // Offset between camera and object.
    private Vector3 offset;

    void Start() {

        // Get camera offset.
        if (objectToFollow) {

            offset = objectToFollow.position - transform.position;
        }
    }

    void Update() {

        // Follow object on Z-axis.
        if (objectToFollow) {

            Vector3 newPos = objectToFollow.position - offset;
            newPos.x = 0;
            transform.position = newPos;
        }
    }
}

