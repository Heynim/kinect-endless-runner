﻿using UnityEngine;
using System.Collections;

// Obstacle object behaviour.
//
public class Obstacle : MonoBehaviour {

    // Sound effect reference.
    private AudioSource soundEffect;

    void Start() {

        soundEffect = GetComponent<AudioSource>();
    }

    // On Player collision, trigger Game Over.
    // Otherwise, destroy gameObject on collision with Destroyer object.
    void OnTriggerEnter(Collider col) {

        if (col.gameObject.tag == Constants.PlayerTag) {
            soundEffect.Play();
            GameManager.Instance.Die();
        }

        if (col.gameObject.tag == Constants.DestroyerTag) {
            Destroy(this.gameObject);
        }
    }
}

